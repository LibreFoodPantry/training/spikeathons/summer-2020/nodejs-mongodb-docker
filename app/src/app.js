const mongoose = require('mongoose');

mongoose.connect('mongodb://mongo/hi-dev', {useNewUrlParser: true})
  .then(() => console.log('MongoDB Connected...'))
  .catch(err => console.log(err));

require('./models/Idea');
const Idea = mongoose.model('ideas');

// CREATE
new Idea({
  title: "oo oo",
  details: "ah man... I just forgot it."
}).save().then(idea => {
  console.log(`Inserted: ${idea}`);

  // READ
  Idea.find({}).then(ideas => {

    // UPDATE
    one_idea = ideas[ideas.length-1];
    one_idea.details = "I just remembered!"
    one_idea.save().then(idea => {

      // READ
      Idea.find({}).then(ideas => {

        if (ideas.length > 3) {
          // DELETE
          ideas[0].remove(() => {

            // READ
            Idea.find({}).then(ideas => {
              console.log(`Found: ${ideas}`);
            });

          });
        } else {
          console.log(`Found: ${ideas}`);
        }

      });

    });
  });
});
