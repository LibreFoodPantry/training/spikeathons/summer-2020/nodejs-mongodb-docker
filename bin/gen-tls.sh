#!/bin/sh

set -eux

mkdir tls
sudo chown :8767 tls
chmod g+s tls
docker run -v "${PWD}/tls/":/tls --rm registry.gitlab.com/librefoodpantry/spikeathons/lfp-spikeathon-summer-2020/gen-tls:2 $(docker-compose ps --services)