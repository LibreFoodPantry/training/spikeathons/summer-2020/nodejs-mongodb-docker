#!/bin/sh

set -eux

docker-compose up --detach mongo
sleep 30
docker-compose run --rm app
