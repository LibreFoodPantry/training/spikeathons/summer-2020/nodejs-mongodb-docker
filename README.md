# nodejs-mongodb-docker

## Dependencies

- Docker
- NodeJS
- Mongoose JS
- MongoDB

## Use

```bash
# Build and start mongo
docker-compose up --detach mongo

# Wait a bit to give MongoDB a chance to start up.
# Then run the app
docker-compose run --rm app

# You should see "Inserted" and "Found"
# followed by some JSON objects.
# Press Ctrl+C to stop the app.
^C

# Feel free to run the app several more times.

# Tear down
docker-compose down --volume --rmi all

# The next command deletes the database's data.
# If you don't delete the data, and rerun the
# statements above, you'll see that the data is
# persisted.
#
# Delete the database's data.
rm -r ./data
```

## Install a new javascript dependency

```bash
# Replace PACKAGE_NAME with the name of the package.
docker-compose run -v "${PWD}":/app -w /app --rm node npm install PACKAGE_NAME
```

## How package.json was initially created

```bash
docker-compose run -v "${PWD}":/app -w /app --rm node npm init
```

## To make `docker-compose run --rm app` respond to signals (^C)

See start script in app/package-json and the CMD statement in app/Dockerfile.

## My log

- Watch Node.js, Express and MongoDB Dev to Deployment by Brad Traversy via O'Reilly Online.
- Created hi.js containing `console.log('Hello');` and then ran `docker run -v "${PWD}":/app -it --rm node:14.3.0-stretch node app/hi` and it prints `Hello`
- Read <https://buddy.works/guides/how-dockerize-node-application>
- Run npm:
  - `docker-compose run -v "${PWD}/app":/app -w /app --rm node npm init`
  - `docker-compose run -v "${PWD}/app":/app -w /app --rm node npm install --save mongoose`
- Read <https://mongoosejs.com/docs/>
- Read <https://nodejs.org/fr/docs/guides/nodejs-docker-webapp/>
- Read <https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md>
- Read <https://blog.risingstack.com/node-js-project-structure-tutorial-node-js-at-scale/>
- Read <https://blog.risingstack.com/node-hero-node-js-project-structure-tutorial/>
- Read <https://hasura.io/blog/an-exhaustive-guide-to-writing-dockerfiles-for-node-js-web-apps-bbee6bd2f3c4/>
- Read <https://nickjanetakis.com/blog/docker-tip-10-project-structure-with-multiple-dockerfiles-and-docker-compose>
- Read <https://github.com/docker-library/healthcheck>
